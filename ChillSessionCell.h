//
//  ChillSessionCell.h
//  ChillKeeper
//
//  Created by Tomáš Hromada on 18/03/15.
//  Copyright (c) 2015 AppVelvet. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Session;
@class PFUser;

@interface ChillSessionCell : UITableViewCell

@property (nonatomic, copy) void (^likesClicked)(Session *session);
@property (nonatomic, copy) void (^userClicked)(PFUser *user);

- (void) setSession:(Session *)session upSession:(Session *)upSession downSession:(Session *)downSession userHidden:(BOOL)userHidden;

@end
