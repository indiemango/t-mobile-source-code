//
//  SettingsListView.h
//  Pictasale
//
//  Created by Tomáš Hromada on 18/03/15.
//  Copyright (c) 2015 pictasale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class SettingsUpdateView;

@interface SettingsListView : UIView < UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate >

@property (nonatomic) UIButton *cancel;
@property (nonatomic) UIButton *done;

@property (nonatomic) SettingsUpdateView *settingsProfileView;

@property (nonatomic, copy) void (^logout)();

- (void) reset;

@end
