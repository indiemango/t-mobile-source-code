//
//  FilterView.m
//  Represent
//
//  Created by Tomáš Hromada on 26/01/15.
//  Copyright (c) 2015 com.represent.represent. All rights reserved.
//

#import "FilterView.h"
#import "UIColor+hexString.h"
#import "FilterButton.h"
#import "FilterScrollViewParent.h"
#import "FilterCampaignsTriangleView.h"

@implementation FilterView
{
    UIButton *done;
    
    FilterButton *profit;
    FilterButton *created;
    FilterButton *ending;
    
    e_sortType sortType;
    e_campaignType campaignType;
    
    FilterScrollViewParent *filterScrollViewParent;
}

@synthesize done, sortType, campaignType;

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 45.0f)];
        
        UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 45.0f)];
        [headerTitle setTextColor:[UIColor colorWithHex:@"#333"]];
        [headerTitle setFont:fHN_R(17)];
        [headerTitle setText:@"Filter"];
        [headerTitle setTextAlignment:NSTextAlignmentCenter];
        
        done = [UIButton buttonWithType:UIButtonTypeCustom];
        [done setFrame:CGRectMake(kScreenWidth - 55, 0, 55, 45.0f)];
        [done.titleLabel setFont:fHN_M(14)];
        [done setTitleColor:[UIColor colorWithHex:@"#4DCAF7"] forState:UIControlStateNormal];
        [done setTitle:@"Done" forState:UIControlStateNormal];
        
        
        [header addSubview:headerTitle];
        [header addSubview:done];
        
        
        UIView *sortByHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 45.0f, kScreenWidth, 35.0f)];
        [sortByHeader setBackgroundColor:[UIColor colorWithHex:@"#ECECEC"]];
        
        UIView *sortByBorder = [[UIView alloc] initWithFrame:CGRectMake(-5.0f, 0.0f, kScreenWidth + 10.0f, 35.0f)];
        [sortByBorder.layer setBorderColor:[[UIColor colorWithHex:@"#D8D8D8"] CGColor]];
        [sortByBorder.layer setBorderWidth:0.5f];
        
        UILabel *sortByTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 0, kScreenWidth - 20.0f, 35.0f)];
        [sortByTitle setTextColor:[UIColor colorWithHex:@"#838383"]];
        [sortByTitle setFont:fHN_M(12)];
        [sortByTitle setText:@"Sort by"];
        
        [sortByHeader addSubview:sortByBorder];
        [sortByHeader addSubview:sortByTitle];
        
        
        
        UIView *sortBy = [[UIView alloc] initWithFrame:CGRectMake(0, 80.0f, kScreenWidth, 75.0f)];
        
        
        profit = [FilterButton buttonWithType:UIButtonTypeCustom];
        [profit setFrame:CGRectMake(kScreenWidth / 2.0f - 140.5f, 22.5f, 93.0f, 30.0f)];
        [profit setTitle:@"Profit" forState:UIControlStateNormal];
        [profit.titleLabel setFont:fHN_R(14)];
        [profit.layer setBorderWidth:0.5f];
        [profit.layer setBorderColor:[[UIColor colorWithHex:@"#393941"] CGColor]];
        
        UIBezierPath *profitMaskPath = [UIBezierPath bezierPathWithRoundedRect:profit.bounds
                                                       byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft
                                                             cornerRadii:CGSizeMake(5.0f, 5.0f)];
        
        CAShapeLayer *profitMaskLayer = [CAShapeLayer layer];
        profitMaskLayer.frame = profit.bounds;
        profitMaskLayer.path = profitMaskPath.CGPath;
        
        profit.layer.mask = profitMaskLayer;
        
        CAShapeLayer *borderLayer = [CAShapeLayer layer];
        borderLayer.frame = profit.bounds;
        borderLayer.path = profitMaskPath.CGPath;
        borderLayer.strokeColor = [UIColor colorWithHex:@"#393941"].CGColor;
        borderLayer.lineWidth = 1.0f;
        borderLayer.fillColor = nil;
        
        [profit.layer addSublayer:borderLayer];
        
        
        [profit setTitleColor:[UIColor colorWithHex:@"#333"] forState:UIControlStateNormal];
        [profit setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [profit setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [profit setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted | UIControlStateSelected];
        
        [profit setBackgroundColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [profit setBackgroundColor:[UIColor colorWithHex:@"#393941"] forState:UIControlStateApplication];
        
        [profit addTarget:self action:@selector(profitDown) forControlEvents:UIControlEventTouchDown];
        
        
        created = [FilterButton buttonWithType:UIButtonTypeCustom];
        [created setFrame:CGRectMake(kScreenWidth / 2.0f - 47.5, 22.5f, 95.0f, 30.0f)];
        [created setTitle:@"Created" forState:UIControlStateNormal];
        [created.titleLabel setFont:fHN_R(14)];
        
        CALayer *topBorder = [CALayer layer];
        topBorder.frame = CGRectMake(0.0f, 0.0f, created.frame.size.width, 0.5f);
        topBorder.backgroundColor = [[UIColor colorWithHex:@"#393941"] CGColor];
        [created.layer addSublayer:topBorder];
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, 29.5f, created.frame.size.width, 0.5f);
        bottomBorder.backgroundColor = [[UIColor colorWithHex:@"#393941"] CGColor];
        [created.layer addSublayer:bottomBorder];
        
        [created setTitleColor:[UIColor colorWithHex:@"#333"] forState:UIControlStateNormal];
        [created setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [created setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [created setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted | UIControlStateSelected];
        
        [created setBackgroundColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [created setBackgroundColor:[UIColor colorWithHex:@"#393941"] forState:UIControlStateApplication];
        
        [created addTarget:self action:@selector(createdDown) forControlEvents:UIControlEventTouchDown];

        
        ending = [FilterButton buttonWithType:UIButtonTypeCustom];
        [ending setFrame:CGRectMake(kScreenWidth / 2.0f + 47.5, 22.5f, 93.0f, 30.0f)];
        [ending setTitle:@"Ending" forState:UIControlStateNormal];
        [ending.titleLabel setFont:fHN_R(14)];

        UIBezierPath *endingMaskPath = [UIBezierPath bezierPathWithRoundedRect:ending.bounds
                                                             byRoundingCorners:UIRectCornerBottomRight | UIRectCornerTopRight
                                                                   cornerRadii:CGSizeMake(5.0f, 5.0f)];
        
        CAShapeLayer *endingMaskLayer = [CAShapeLayer layer];
        endingMaskLayer.frame = ending.bounds;
        endingMaskLayer.path = endingMaskPath.CGPath;
        
        ending.layer.mask = endingMaskLayer;
        
        
        borderLayer = [CAShapeLayer layer];
        borderLayer.frame = ending.bounds;
        borderLayer.path = endingMaskPath.CGPath;
        borderLayer.strokeColor = [UIColor colorWithHex:@"#393941"].CGColor;
        borderLayer.lineWidth = 1.0f;
        borderLayer.fillColor = nil;
        
        [ending.layer addSublayer:borderLayer];
        
        
        [ending setTitleColor:[UIColor colorWithHex:@"#333"] forState:UIControlStateNormal];
        [ending setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [ending setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [ending setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted | UIControlStateSelected];
        
        [ending setBackgroundColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [ending setBackgroundColor:[UIColor colorWithHex:@"#393941"] forState:UIControlStateApplication];
        
        [ending addTarget:self action:@selector(endingDown) forControlEvents:UIControlEventTouchDown];
        
        
        [sortBy addSubview:profit];
        [sortBy addSubview:ending];
        [sortBy addSubview:created];
        
        [profit setSelected:YES];
        
        
        
        UIView *filterCampaignsHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 155.0f, kScreenWidth, 35.0f)];
        [filterCampaignsHeader setBackgroundColor:[UIColor colorWithHex:@"#ECECEC"]];
        
        UIView *filterCampaignsBorder = [[UIView alloc] initWithFrame:CGRectMake(-5.0f, 0.0f, kScreenWidth + 10.0f, 35.0f)];
        [filterCampaignsBorder.layer setBorderColor:[[UIColor colorWithHex:@"#D8D8D8"] CGColor]];
        [filterCampaignsBorder.layer setBorderWidth:0.5f];
        
        UILabel *filterCampaignsTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 0, kScreenWidth - 20.0f, 35.0f)];
        [filterCampaignsTitle setTextColor:[UIColor colorWithHex:@"#838383"]];
        [filterCampaignsTitle setFont:fHN_M(12)];
        [filterCampaignsTitle setText:@"Filter campaigns"];
        
        [filterCampaignsHeader addSubview:filterCampaignsBorder];
        [filterCampaignsHeader addSubview:filterCampaignsTitle];
        
        
        
        filterScrollViewParent = [[FilterScrollViewParent alloc] initWithFrame:CGRectMake(0, 190.0f, kScreenWidth, 60.0f)];
        
        filterScrollViewParent.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(kScreenWidth / 2.0f - 72.5f, 0, 145.0f, 60.0f)];
        [filterScrollViewParent.scrollView setContentSize:CGSizeMake(kNumberOfCampaignsType * 145.0f, 60.0f)];
        [filterScrollViewParent.scrollView setPagingEnabled:YES];
        [filterScrollViewParent.scrollView setClipsToBounds:NO];
        [filterScrollViewParent.scrollView setShowsHorizontalScrollIndicator:NO];
        [filterScrollViewParent.scrollView setDelegate:self];
        
        
        for (NSUInteger i = 0; i < kNumberOfCampaignsType; i++)
        {
        
            UILabel *filterCampaignsTitle = [[UILabel alloc] initWithFrame:CGRectMake(i * 145, 0, 145, 60.0f)];
            [filterCampaignsTitle setFont:fHN_R(14)];
            [filterCampaignsTitle setTextColor:[UIColor colorWithHex:@"#393941"]];
            [filterCampaignsTitle setTextAlignment:NSTextAlignmentCenter];
            
            switch (i)
            {
                case 0:
                    [filterCampaignsTitle setText:@"All campaigns"];
                    break;
                case 1:
                    [filterCampaignsTitle setText:@"Active campaigns"];
                    break;
                case 2:
                    [filterCampaignsTitle setText:@"Tilted campaigns"];
                    break;
                case 3:
                    [filterCampaignsTitle setText:@"Finished campaigns"];
                    break;
                case 4:
                    [filterCampaignsTitle setText:@"Shipped campaigns"];
                    break;
            }
            
            [filterScrollViewParent.scrollView addSubview:filterCampaignsTitle];
        }
        
        
        FilterCampaignsTriangleView *triangle = [[FilterCampaignsTriangleView alloc] initWithFrame:CGRectMake(kScreenWidth / 2.0f - 7.5f, 0.0f, 15.0f, 15.0f)];
        
        [filterScrollViewParent addSubview:filterScrollViewParent.scrollView];
        [filterScrollViewParent addSubview:triangle];
        
        
        [self addSubview:header];
        [self addSubview:sortByHeader];
        [self addSubview:sortBy];
        [self addSubview:filterCampaignsHeader];
        [self addSubview:filterScrollViewParent];
    }
    return self;
}

#pragma mark - Public methods

- (void) reset
{
    [filterScrollViewParent.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    campaignType = eCampaignAll;
    [self profitDown];
}

#pragma mark - UIScrollView delegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    campaignType = roundf((scrollView.contentOffset.x) / 145.0f);
}

#pragma mark - private methods

- (void) profitDown
{
    sortType = eSortProfit;
    [profit setSelected:YES];
    [created setSelected:NO];
    [ending setSelected:NO];
}

- (void) createdDown
{
    sortType = eSortCreated;
    [profit setSelected:NO];
    [created setSelected:YES];
    [ending setSelected:NO];
}

- (void) endingDown
{
    sortType = eSortEnding;
    [profit setSelected:NO];
    [created setSelected:NO];
    [ending setSelected:YES];
}

@end
