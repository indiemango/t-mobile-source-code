//
//  ChillSessionCell.m
//  ChillKeeper
//
//  Created by Tomáš Hromada on 18/03/15.
//  Copyright (c) 2015 AppVelvet. All rights reserved.
//

#import "ChillSessionCell.h"
#import "Session.h"

#import "UIColor+Interpolate.h"

@implementation ChillSessionCell
{
    Session *session;
    
    UIImageView *backgroundImage;
    
    UIImageView *profileImage;
    UIButton *profileImageButton;
    
    UIView *chillCircle;
    UIImageView *chillIcon;
    
    CGFloat chillBottom;
    
    UILabel *mainTitle;
    UILabel *secondaryTitle;
    
    UIButton *loveButton;
    
    UIView *gradientLineUp;
    UIView *gradientLineDown;
    
    CAGradientLayer *gradientLayerUp;
    CAGradientLayer *gradientLayerDown;
    
    void (^likesClicked)(Session *session);
    void (^userClicked)(PFUser *user);
}

@synthesize likesClicked, userClicked;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth / 2.0f - 21, 12, 42, 42)];
        
        profileImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [profileImageButton setFrame:CGRectMake(kScreenWidth / 2.0f - 21, 12, 42, 42)];
        [profileImageButton addTarget:self action:@selector(profileImageClicked) forControlEvents:UIControlEventTouchUpInside];
        
        chillCircle = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth / 2.0f, 23, 0, 0)];
        
        chillBottom = chillCircle.frame.origin.y + chillCircle.frame.size.height;
        
        mainTitle = [[UILabel alloc] init];
        secondaryTitle = [[UILabel alloc] init];
        
        loveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [loveButton addTarget:self action:@selector(loveClicked) forControlEvents:UIControlEventTouchUpInside];
        
        gradientLineUp = [[UIView alloc] init];
        gradientLineDown = [[UIView alloc] init];
        
        gradientLayerUp = [[CAGradientLayer alloc] init];
        gradientLayerDown = [[CAGradientLayer alloc] init];
        
        [gradientLineUp.layer insertSublayer:gradientLayerUp atIndex:0];
        [gradientLineDown.layer insertSublayer:gradientLayerDown atIndex:0];
        
        [self addSubview:gradientLineUp];
        [self addSubview:gradientLineDown];
        
        [self addSubview:chillCircle];
        [self addSubview:chillIcon];
        
        [self addSubview:profileImage];
        [self addSubview:profileImageButton];
        [self addSubview:mainTitle];
        [self addSubview:secondaryTitle];
        [self addSubview:loveButton];
        
        [self setLinkedFrames];
        
    }
    return self;
}

- (void) loveClicked
{
    likesClicked(session);
}

- (void) profileImageClicked
{
    userClicked([session user]);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setLinkedFrames
{
    [mainTitle setFrame:CGRectMake(10, chillBottom + 46, kScreenWidth - 20, 20)];
    [secondaryTitle setFrame:CGRectMake(10, chillBottom + 74, kScreenWidth - 20, 17)];
    
    [loveButton setFrame:CGRectMake(kScreenWidth / 2.0f - 25, chillBottom + 101, 50, 24)];
    
    [gradientLineUp setFrame:CGRectMake(kScreenWidth / 2.0f - 1.0f, 0, 2.0f, chillBottom)];
    [gradientLineDown setFrame:CGRectMake(kScreenWidth / 2.0f - 1.0f, chillBottom, 2.0f, 125)];
}

- (void) setSession:(Session *)pSession upSession:(Session *)upSession downSession:(Session *)downSession userHidden:(BOOL)userHidden
{
    session = pSession;
    
    
    [self setUpSession:upSession downSession:downSession];
    
    if (userHidden)
    {
        [profileImage setHidden:YES];
        [profileImageButton setHidden:YES];
    }
    else
    {
        [profileImage setHidden:NO];
        [profileImageButton setHidden:NO];
    }
//    profileImage setImage:[session user][@"profilePicture"];
    
    
    NSString *username = [session user][@"name"];
    NSString *place = [session place];
    NSString *location = [session location];
    
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger units = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *chillDurationComponents = [gregorian components:units fromDate:session.start toDate:session.end options:0];
    
    NSInteger chillDurationHours = [chillDurationComponents hour];
    NSInteger chillDurationMinutes = [chillDurationComponents minute];
    
    CGFloat chillCircleDiameter = 0.0f;
    
    if (chillDurationHours > 0 || chillDurationMinutes > 50) chillCircleDiameter = 300.0f;
    else if (chillDurationMinutes > 40) chillCircleDiameter = 250.0f;
    else if (chillDurationMinutes > 30) chillCircleDiameter = 200.0f;
    else if (chillDurationMinutes > 20) chillCircleDiameter = 150.0f;
    else if (chillDurationMinutes > 10) chillCircleDiameter = 100.0f;
    else chillCircleDiameter = 72.5f;
    
    [chillCircle setFrame:CGRectMake(kScreenWidth / 2.0f - chillCircleDiameter / 2.0f, 23.0f, chillCircleDiameter, chillCircleDiameter)];
    
    // add uiimage from svg with the chillCircleDiameter width/height
    chillBottom = chillCircle.frame.origin.y + chillCircle.frame.size.height;
    
    [self setLinkedFrames];
    
    // add icons for hours
    
    NSMutableAttributedString *mainAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ chilled %@ %@", username, (chillDurationHours == 0 ? [NSString stringWithFormat:@"%li'", (long)chillDurationMinutes] : [NSString stringWithFormat:@"%li:%li'", (long)chillDurationHours, (long)chillDurationMinutes]), place]];
    [mainAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, username.length)];
    [mainAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor yellowColor] range:NSMakeRange(username.length, mainAttributedString.length - username.length)];
    
    [mainTitle setAttributedText:mainAttributedString];
    
    
    NSDateComponents *chillEndComponents = [gregorian components:units fromDate:session.end toDate:[NSDate date] options:0];
    
    NSInteger chillEndYears = [chillEndComponents year];
    NSInteger chillEndMonths = [chillEndComponents month];
    NSInteger chillEndDays = [chillEndComponents day];
    NSInteger chillEndHours = [chillEndComponents hour];
    NSInteger chillEndMintues = [chillEndComponents minute];
    NSInteger chillEndSeconds = [chillEndComponents second];
    
    NSString *dateString = @"";
    NSInteger dateNumber = 0;
    
    if (chillEndYears > 0)
    {
        dateNumber = chillEndYears;
        dateString = @"y";
//        dateString = [NSString stringWithFormat:@"year%@", dateNumber == 1 ? @"" : @"s"];
    }
    else if (chillEndMonths > 0)
    {
        dateNumber = chillEndMonths;
        dateString = [NSString stringWithFormat:@"mon%@", dateNumber == 1 ? @"" : @"s"];
    }
    else if (chillEndDays > 0)
    {
        dateNumber = chillEndDays;
        dateString = @"d";
//        dateString = [NSString stringWithFormat:@"day%@", dateNumber == 1 ? @"" : @"s"];
    }
    else if (chillEndHours > 0)
    {
        dateNumber = chillEndHours;
        dateString = @"h";
//        dateString = [NSString stringWithFormat:@"hour%@", dateNumber == 1 ? @"" : @"s"];
    }
    else if (chillEndMintues > 0)
    {
        dateNumber = chillEndMintues;
        dateString = [NSString stringWithFormat:@"min%@", dateNumber == 1 ? @"" : @"s"];
    }
    else
    {
        dateNumber = chillEndSeconds;
        dateString = @"s";
//        dateString = [NSString stringWithFormat:@"second%@", dateNumber == 1 ? @"" : @"s"];
    }
    
    
    /*
    1s ago
    1min ago
    3mins ago
    3h ago
    3d ago
    1mon ago
    3mons ago
    3y ago
     */
    
    NSString *chillEndTimeString = [NSString stringWithFormat:@"%li%@ ago", (long)dateNumber, dateString];
    
    NSMutableAttributedString *secondaryAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ in %@", chillEndTimeString, location]];
    
    [secondaryAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0, chillEndTimeString.length)];
    [secondaryAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(chillEndTimeString.length, secondaryAttributedString.length - chillEndTimeString.length)];
    
    [secondaryTitle setAttributedText:secondaryAttributedString];
    
    
}

- (void) setUpSession:(Session *)upSession downSession:(Session *)downSession
{
    
    // todo remove layer
    
    if (!upSession) [gradientLayerUp setColors:@[(id)[UIColor clearColor].CGColor]];
    else
    {
        // add gradient for colors of upSession->session
        
        [gradientLayerUp setBounds:gradientLineUp.bounds];
        [gradientLayerUp setColors:@[(id)[UIColor colorBetweenColorA:dChillColors[upSession.chillType] colorB:dChillColors[session.chillType]].CGColor, (id)[dChillColors[session.chillType] CGColor]]];
        
    }
    
    if (!downSession) [gradientLayerDown setColors:@[(id)[UIColor clearColor].CGColor]];
    else
    {
        // add gradient for colors of session->downSession
        
        [gradientLayerDown setBounds:gradientLineDown.bounds];
        [gradientLayerDown setColors:@[(id)[dChillColors[session.chillType] CGColor], (id)[UIColor colorBetweenColorA:dChillColors[session.chillType] colorB:dChillColors[upSession.chillType]].CGColor]];
        
    }
    
}

@end
