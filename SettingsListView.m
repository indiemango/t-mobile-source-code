//
//  SettingsListView.m
//  Pictasale
//
//  Created by Tomáš Hromada on 18/03/15.
//  Copyright (c) 2015 pictasale. All rights reserved.
//

#import "SettingsListView.h"
#import <Parse/Parse.h>

#import "SettingsUpdateView.h"
#import "SettingsListCell.h"

@implementation SettingsListView
{
    UIButton *cancel;
    UIButton *done;
    
    UITableView *settingsTableView;
    SettingsUpdateView *settingsProfileView;
    
    void (^logout)();
}

@synthesize cancel, done, settingsProfileView, logout;

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        
        [self setClipsToBounds:YES];
        [self setBackgroundColor:[UIColor colorWithHexString:@"#eef4f5"]];
        
        UIView *navbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, kNavbarHeight)];
        [navbar setBackgroundColor:DarkGrayColor];
        
        UILabel *navbarTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screenWidth, kNavbarHeight)];
        [navbarTitle setTextColor:[UIColor whiteColor]];
        [navbarTitle setTextAlignment:NSTextAlignmentCenter];
        [navbarTitle setBackgroundColor:[UIColor clearColor]];
        [navbarTitle setUserInteractionEnabled:NO];
        [navbarTitle setFont:[UIFont fontWithName:ANB size:18]];
        [navbarTitle setText:@"Settings"];
        
        cancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancel setFrame:CGRectMake(0, 0, 50, kNavbarHeight)];
        [cancel setImage:[UIImage imageNamed:@"navbar_cancel"] forState:UIControlStateNormal];
        [cancel setImage:[UIImage imageNamed:@"navbar_cancel_down"] forState:UIControlStateHighlighted];
        
        done = [UIButton buttonWithType:UIButtonTypeCustom];
        [done setFrame:CGRectMake(screenWidth - 50, 0, 50, kNavbarHeight)];
        [done setImage:[UIImage imageNamed:@"navbar_done"] forState:UIControlStateNormal];
        [done setImage:[UIImage imageNamed:@"navbar_done_down"] forState:UIControlStateHighlighted];
        [done addTarget:settingsProfileView action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
        
        [navbar addSubview:navbarTitle];
        [navbar addSubview:cancel];
        [navbar addSubview:done];
        
        
        settingsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, screenWidth, screenHeight - kNavbarHeight)];
        [settingsTableView setDataSource:self];
        [settingsTableView setDelegate:self];
        [settingsTableView setBackgroundColor:[UIColor colorWithHexString:@"#eef4f5"]];
        [settingsTableView setBounces:NO];
        [settingsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [settingsTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 1.0f)]];
        
        
        settingsProfileView = [[SettingsUpdateView alloc] initWithFrame:CGRectMake(0, kNavbarHeight, screenWidth, screenHeight - kNavbarHeight)];
        
        
        
        [self addSubview:navbar];
        [self addSubview:settingsTableView];
        
    }
    return self;
}

- (void) reset
{
    [settingsTableView setFrame:CGRectMake(0, kNavbarHeight, screenWidth, screenHeight - kNavbarHeight)];
    [settingsProfileView removeFromSuperview];
}

- (void) openProfileSettings
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kControlBarHideNotification object:nil];
    
    [settingsProfileView setFrame:CGRectMake(screenWidth, kNavbarHeight, screenWidth, screenHeight - kNavbarHeight)];
    [self addSubview:settingsProfileView];
    
    [UIView animateWithDuration:0.3f animations:^{
        [settingsTableView setFrame:CGRectMake(-screenWidth, kNavbarHeight, screenWidth, screenHeight - kNavbarHeight)];
        [settingsProfileView setFrame:CGRectMake(0, kNavbarHeight, screenWidth, screenHeight - kNavbarHeight)];
    }];
}

- (void) openMail
{
    MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
    
    [mailController setMailComposeDelegate:self];
    [mailController setToRecipients:@[@"hello@pictasale.com"]];
    [mailController setSubject:@"Hello"];
    [mailController setMessageBody:@"" isHTML:NO];
    
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:mailController animated:YES completion:nil];
}

- (void) sendToAppStore
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kRateUsURL]];
}

#pragma mark - MFMailComposeViewController method

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) return 2;
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kSettingsListCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kSettingsListSectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, kSettingsListSectionHeaderHeight)];
    [header setBackgroundColor:[UIColor colorWithHexString:@"#eef4f5"]];
    
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(22, 15, screenWidth - 22, kSettingsListSectionHeaderHeight - 15)];
    [text setFont:[UIFont fontWithName:ANR size:14]];
    [text setTextColor:DarkGrayColor];
    
    switch (section)
    {
        case 0:
            [text setText:@"PROFILE"];
            break;
        case 1:
            [text setText:@"SUPPORT & FEEDBACK"];
            break;
        case 2:
            [text setText:@"LOGOUT"];
            break;
    }
    
    [header addSubview:text];
    
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SettingsListCell";
    
    SettingsListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) cell = [[SettingsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    
    [cell.arrow setHidden:YES];
    
    switch ([indexPath section])
    {
        case 0:
            [cell.image setImage:[UIImage imageNamed:@"settings_list_cell_icon_profile"]];
            [cell.text setText:[PFUser currentUser][@"name"]];
            [cell.arrow setHidden:NO];
            break;
        case 1:
            switch ([indexPath row])
        {
            case 0:
                [cell.image setImage:[UIImage imageNamed:@"settings_list_cell_icon_support"]];
                [cell.text setText:@"hello@pictasale.com"];
                break;
            case 1:
                [cell.image setImage:[UIImage imageNamed:@"settings_list_cell_icon_rate"]];
                [cell.text setText:@"Rate us on the app store"];
                break;
        }
            break;
        case 2:
            [cell.image setImage:[UIImage imageNamed:@"settings_list_cell_icon_logout"]];
            [cell.text setText:@"Logout"];
            break;
    }
    
    
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([indexPath section])
    {
        case 0:
            [self openProfileSettings];
            break;
        case 1:
            switch ([indexPath row])
        {
            case 0:
                [self openMail];
                break;
            case 1:
                [self sendToAppStore];
                break;
        }
            break;
        case 2:
            logout();
            break;
    }
    return indexPath;
}

@end
