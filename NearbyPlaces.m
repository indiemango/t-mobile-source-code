//
//  NearbyPlaces.m
//  Pictasale
//
//  Created by Tomáš Hromada on 23/07/14.
//  Copyright (c) 2014 pictasale. All rights reserved.
//

#import "NearbyPlaces.h"

@implementation NearbyPlaces

NSString * const kFoursquareURL = @"https://api.foursquare.com/v2/venues";
NSString * const kFoursquareCLIENT_ID = @"TNAUUVC5NX1VTJ05PSBYUUWRUZRHJHDZWYCUMYZ2RPN4XDII";
NSString * const kFoursquareCLIENT_SECRET = @"MDEIPR1SIOMON215YTHBPFV4MIY5WCDSVZ3DDGZ02S3PF2WS";
NSString * const kFoursquareVersion = @"20140806";

NSString * const kYelpURL = @"http://api.yelp.com/v2";
NSString * const kYelpCONSUMER_KEY = @"IlSHVkxyAxZhXiFbyR5DMQ";
NSString * const kYelpCONSUMER_SECRET = @"pnEP3ZqIx6RaCAawJn-rsTCKxms";
NSString * const kYelpTOKEN = @"M3b6-ia7tWMlkjFHz7T_BXPDdAdhmmcC";
NSString * const kYelpTOKEN_SECRET = @"twx_ZDY2WHfuWrKDJdzxF3uuWnY";

NSString * const kGPlacesURL = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json";
NSString * const kGPlacesSERVER_KEY = @"AIzaSyAqOtzyYTUSunSlRsjbJ6beMg-5btos7EA";
NSString * const kGPlacesTYPES = @"accounting|airport|amusement_park|aquarium|art_gallery|atm|bakery|bank|bar|beauty_salon|bicycle_store|book_store|bowling_alley|bus_station|cafe|campground|car_dealer|car_rental|car_repair|car_wash|casino|cemetery|church|city_hall|clothing_store|convenience_store|courthouse|dentist|department_store|doctor|electrician|electronics_store|embassy|establishment|finance|fire_station|florist|food|funeral_home|furniture_store|gas_station|general_contractor|grocery_or_supermarket|gym|hair_care|hardware_store|health|hindu_temple|home_goods_store|hospital|insurance_agency|jewelry_store|laundry|lawyer|library|liquor_store|local_government_office|locksmith|lodging|meal_delivery|meal_takeaway|mosque|movie_rental|movie_theater|moving_company|museum|night_club|painter|park|parking|pet_store|pharmacy|physiotherapist|place_of_worship|plumber|police|post_office|real_estate_agency|restaurant|roofing_contractor|rv_park|school|shoe_store|shopping_mall|spa|stadium|storage|store|subway_station|synagogue|taxi_stand|train_station|travel_agency|university|veterinary_care|zoo";

NSInteger const kLimit = 20;

- (instancetype)init
{
    if (self = [super init])
    {
        
        foursquareClient = [AFHTTPRequestOperationManager manager];
        foursquareClient.responseSerializer = [AFJSONResponseSerializer serializer];
     
        
        yelpClient = [[BDBOAuth1RequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:kYelpURL] consumerKey:kYelpCONSUMER_KEY consumerSecret:kYelpCONSUMER_SECRET];
        yelpClient.responseSerializer = [AFJSONResponseSerializer serializer];
        
        BDBOAuthToken *yelpToken = [[BDBOAuthToken alloc] init];
        [yelpToken setToken:kYelpTOKEN];
        [yelpToken setSecret:kYelpTOKEN_SECRET];
        
        [yelpClient.requestSerializer saveAccessToken:yelpToken];
        [yelpClient.requestSerializer setRequestToken:yelpToken];
        
        
        gplacesClient = [AFHTTPRequestOperationManager manager];
        gplacesClient.responseSerializer = [AFJSONResponseSerializer serializer];
        
    }
    return self;
}

- (void) FOURSQUARE_exploreNearbyPlacesOnLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure
{
    NSDictionary *params = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%f,%f", latitude, longitude], [NSNumber numberWithInteger:kLimit], kFoursquareCLIENT_ID, kFoursquareCLIENT_SECRET, kFoursquareVersion, @"foursquare"] forKeys:@[@"ll", @"limit", @"client_id", @"client_secret", @"v", @"m"]];
    
    [foursquareClient GET:[NSString stringWithFormat:@"%@/explore", kFoursquareURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
        NSLog(@"%@", error.description);
    }];
}

- (void) FOURSQUARE_searchNerbyPlacesOnLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure
{
    NSDictionary *params = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%f,%f", latitude, longitude], [NSNumber numberWithInteger:kLimit], kFoursquareCLIENT_ID, kFoursquareCLIENT_SECRET, kFoursquareVersion, @"foursquare"] forKeys:@[@"ll", @"limit", @"client_id", @"client_secret", @"v", @"m"]];
    
    [foursquareClient GET:[NSString stringWithFormat:@"%@/search", kFoursquareURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
        NSLog(@"%@", error.description);
    }];
}

- (void) YELP_searchNearbyPlacesOnLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure
{
    NSDictionary *OAuthParams = [yelpClient.requestSerializer OAuthParameters];
    
    NSDictionary *weakParams = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%f,%f", latitude, longitude], [NSNumber numberWithInteger:kLimit], kYelpCONSUMER_KEY, kYelpTOKEN, [OAuthParams objectForKey:@"oauth_signature_method"], [OAuthParams objectForKey:@"oauth_timestamp"], [OAuthParams objectForKey:@"oauth_nonce"]] forKeys:@[@"ll", @"limit", @"oauth_consumer_key", @"oauth_token", @"oauth_signature_method", @"oauth_timestamp", @"oauth_nonce"]];
    
    NSString *signature = [yelpClient.requestSerializer OAuthSignatureForMethod:@"GET" URLString:[NSString stringWithFormat:@"%@/search", kYelpURL] parameters:weakParams error:nil];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%f,%f", latitude, longitude], [NSNumber numberWithInteger:kLimit], kYelpCONSUMER_KEY, kYelpTOKEN, [OAuthParams objectForKey:@"oauth_signature_method"], signature, [OAuthParams objectForKey:@"oauth_timestamp"], [OAuthParams objectForKey:@"oauth_nonce"]] forKeys:@[@"ll", @"limit", @"oauth_consumer_key", @"oauth_token", @"oauth_signature_method", @"oauth_signature", @"oauth_timestamp", @"oauth_nonce"]];
    
    [yelpClient GET:@"search" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
        NSLog(@"%@", error.description);
        NSLog(@"%@", [operation responseString]);
    }];
}

- (void) GPLACES_searchNearbyPlacesOnLattitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure
{
    NSDictionary *params = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%f,%f", latitude, longitude], @"distance", kGPlacesTYPES, kGPlacesSERVER_KEY] forKeys:@[@"location", @"rankby", @"types", @"key"]];
    
    [foursquareClient GET:kGPlacesURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
        NSLog(@"%@", error.description);
        NSLog(@"%@", operation.request.URL);
    }];
}

- (void) FACEBOOK_searchNerabyPlacesOnLattitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure
{
    FBRequestConnection *connection = [[FBRequestConnection alloc] init];
    
    FBRequest *request = [FBRequest requestForPlacesSearchAtCoordinate:CLLocationCoordinate2DMake(latitude, longitude) radiusInMeters:50000 resultsLimit:kLimit searchText:@""];

    [connection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (error == nil) completion(result);
        else
        {
            failure(error);
            NSLog(@"%@", error.description);
        }
    }];
    
    [connection start];
}

@end
