//
//  NearbyPlaces.h
//  Pictasale
//
//  Created by Tomáš Hromada on 23/07/14.
//  Copyright (c) 2014 pictasale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
#import "AFHTTPRequestOperationManager.h"
#import "BDBOAuth1RequestOperationManager.h"


@interface NearbyPlaces : NSObject
{
    AFHTTPRequestOperationManager *foursquareClient;
    
    BDBOAuth1RequestOperationManager *yelpClient;
    
    AFHTTPRequestOperationManager *gplacesClient;
}


///-----------------
/// @name FOURSQUARE
///-----------------

/**
 Returns a list of nearby places returned from 'explore' function.
 Read on the documentation to see if it fits your need more than 'search'.
 
 @param latitude    The latitude of the location, from where the search is made.
 @param longitude   The longitude of the location from where the search is made.
 @param completion  The completion block, in case of a succesfull call.
 */

- (void) FOURSQUARE_exploreNearbyPlacesOnLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure;

/**
 Returns a list of nearby places returned from 'search' function.
 Read on the documentation to see if it fits your need more than 'explore'.
 
 @param latitude    The latitude of the location, from where the search is made.
 @param longitude   The longitude of the location from where the search is made.
 @param completion  The completion block, in case of a succesfull call.
 */

- (void) FOURSQUARE_searchNerbyPlacesOnLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure;


///-----------
/// @name YELP
///-----------

/**
 Returns a list of nearby places returned from Yelp API.
 
 @param latitude    The latitude of the location, from where the search is made.
 @param longitude   The longitude of the location from where the search is made.
 @param completion  The completion block, in case of a succesfull call.
 */

- (void) YELP_searchNearbyPlacesOnLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure;


///--------------------
/// @name GOOGLE PLACES
///--------------------

/**
 Returns a list of nearby places returned from Google Places API.
 
 @param latitude    The latitude of the location, from where the search is made.
 @param longitude   The longitude of the location from where the search is made.
 @param completion  The completion block, in case of a succesfull call.
 */

- (void) GPLACES_searchNearbyPlacesOnLattitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure;


///----------------------
/// @name FACEBOOK PLACES
///----------------------

/**
 Returns a list of nearby places returned from Facebook Places API.
 
 @param latitude    The latitude of the location, from where the search is made.
 @param longitude   The longitude of the location from where the search is made.
 @param completion  The completion block, in case of a succesfull call.
 */

- (void) FACEBOOK_searchNerabyPlacesOnLattitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void(^)(NSDictionary *results))completion withFailureBlock:(void(^)(NSError *error))failure;

@end
