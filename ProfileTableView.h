//
//  ProfileTableView.h
//  ChillKeeper
//
//  Created by Tomáš Hromada on 18/03/15.
//  Copyright (c) 2015 AppVelvet. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Session;
@class PFUser;

@interface ProfileTableView : UITableView < UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate >

@property (nonatomic, copy) void (^likesClicked)(Session *session);

- (instancetype)initWithFrame:(CGRect)frame user:(PFUser *)user;
- (void) setUser:(PFUser *)pUser;

@end
