//
//  ProfileTableView.m
//  ChillKeeper
//
//  Created by Tomáš Hromada on 18/03/15.
//  Copyright (c) 2015 AppVelvet. All rights reserved.
//

#import "ProfileTableView.h"

#import <Parse/Parse.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Session.h"
#import "UIScrollView+InfiniteScroll.h"
#import "UIColor+hexString.h"
#import "UIImage+imageScaledToSize.h"

#import "ChillSessionCell.h"


@implementation ProfileTableView
{
    PFUser *user;
    
    NSUInteger skip;
    NSArray *sessions;
    
    
    UITextField *username;
    UIImageView *profileImage;
    
    UIActivityIndicatorView *profileImageIndicator;
    UIActivityIndicatorView *usernameIndicator;
    
    
    void (^likesClicked)(Session *session);
}

@synthesize likesClicked;

- (instancetype)initWithFrame:(CGRect)frame user:(PFUser *)pUser
{
    if (self = [super initWithFrame:frame])
    {
        
        [self setUser:pUser];
        
        __weak typeof(self) weakSelf = self;
        
        [self addInfiniteScrollWithHandler:^(UIScrollView *scrollView) {
            __strong typeof(self) strongSelf = weakSelf;
            
            [strongSelf query];
        }];
    }
    return self;
}

- (void) setUser:(PFUser *)pUser
{
    user = pUser;
    
    sessions = nil;
    skip = 0;
    
    [self reloadData];
    [self query];
}

#pragma mark - Load methods

- (void) query
{
    PFQuery *query = [Session query];
    [query orderByDescending:@"createdAt"];
    [query whereKey:@"user" equalTo:user];
    [query setLimit:10];
    [query setSkip:skip];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if (objects.count == 0) return;
            
            skip += objects.count;
            
            sessions = [sessions arrayByAddingObjectsFromArray:objects];
            [self reloadData];
        }
        else
        {
            NSLog(@"TODO add error alert - session query in profileTableView");
        }
    }];
}

#pragma mark - Save and Update methods

- (void) savePicture:(UIImage *)picture
{
    
    if (picture != nil)
    {
        [self startAnimatingIndicator:profileImageIndicator];
        NSData *data = UIImagePNGRepresentation(picture);
        
        PFFile *profilePhoto = [PFFile fileWithName:@"profilePhoto.png" data:data];
        [profilePhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded)
            {
                [PFUser currentUser][@"profilePhoto"] = profileImage;
                [[PFUser currentUser] saveInBackground];
            }
            [self stopAnimatingIndicator:profileImageIndicator];
        }];
        
        [profileImage setImage:picture];
    }

}

- (void) saveName:(NSString *)name
{
    [self startAnimatingIndicator:usernameIndicator];
    NSString *namePrev = user[@"name"];
    user[@"name"] = name;
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) [username setText:name];
        else
        {
            [username setText:namePrev];
            NSLog(@"TODO show something, error saving name: %@", error.description);
        }
        [self stopAnimatingIndicator:usernameIndicator];
    }];
}

#pragma mark - Photo update methods


- (void) changePicture
{
    
    if ([UIAlertController class])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Where to pick a new photo from?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Photo library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self showImagePicker];
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self showCamera];
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Where to pick a new photo from?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo library", @"Camera", nil];
        [actionSheet showInView:self];
    }
    
}

- (void) setNewImage:(UIImage *)image
{
    [self savePicture:image];
}

- (void) showImagePicker
{
    UIImagePickerController *picker;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [picker setAllowsEditing:YES];
        [self.window.rootViewController presentViewController:picker animated:YES completion:nil];
    }
}

- (void) showCamera
{
    UIImagePickerController *picker;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [picker setAllowsEditing:YES];
        [self.window.rootViewController presentViewController:picker animated:YES completion:nil];
    }
}

#pragma mark - UIActionSheet delegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self showImagePicker];
            break;
        case 1:
            [self showCamera];
            break;
    }
}

#pragma mark - UIImagePickerController delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToUse;
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        if (editedImage) imageToUse = editedImage;
        else imageToUse = originalImage;
        
        
        if (imageToUse.size.width > imageToUse.size.height) [self setNewImage:[imageToUse imageScaledToSize:CGSizeMake(imageToUse.size.width * profileImage.frame.size.width / imageToUse.size.height, profileImage.frame.size.width)]];
        else [self setNewImage:[imageToUse imageScaledToSize:CGSizeMake(profileImage.frame.size.width, imageToUse.size.height * profileImage.frame.size.width / imageToUse.size.width)]];
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Indicator methods

- (void) startAnimatingIndicator:(UIActivityIndicatorView *)indicator
{
    [indicator startAnimating];
    [UIView animateWithDuration:0.3f animations:^{
        [indicator setAlpha:1.0f];
    }];
}

- (void) stopAnimatingIndicator:(UIActivityIndicatorView *)indicator
{
    [UIView animateWithDuration:0.3f animations:^{
        [indicator setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [indicator stopAnimating];
    }];
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (![textField.text isEqualToString:@""] && [textField.text isEqualToString:user[@"name"]]) [self saveName:textField.text];
    else [textField setText:user[@"name"]];
    return NO;
}

#pragma mark - UITableView delegate & dataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return sessions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kProfileTableViewHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kProfileTableViewHeaderHeight)];
    
    
    profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth / 2.0f - 82.5, 25.0f, 165, 165)];
    [profileImage.layer setCornerRadius:82.5f];
    [profileImage setContentMode:UIViewContentModeScaleAspectFill];
    
    profileImageIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [profileImageIndicator setFrame:CGRectMake(kScreenWidth / 2.0f - 82.5, 25.0f, 165, 165)];
    [profileImageIndicator setAlpha:0.0f];
    
    
    username = [[UITextField alloc] initWithFrame:CGRectMake(10, 200.0f, kScreenWidth - 10.0f, 30.0f)];
    [username setFont:fHNBI(15)];
    [username setTextColor:[UIColor whiteColor]];
    [username setDelegate:self];
    [username setUserInteractionEnabled:[user.objectId isEqualToString:[PFUser currentUser].objectId]];
    
    usernameIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [usernameIndicator setFrame:CGRectMake(kScreenWidth - 10.0f, 200.0f, 10.0f, 30.0f)];
    [usernameIndicator setAlpha:0.0f];
    
    
    [header addSubview:profileImage];
    [header addSubview:profileImageIndicator];
    [header addSubview:username];
    [header addSubview:usernameIndicator];
    
    // bottom info bar: session count, number of hours
    
    UIView *purpleLine = [[UIView alloc] initWithFrame:CGRectMake(0, kProfileTableViewHeaderHeight - 35.0f, kScreenWidth, 1.0f)];
    [purpleLine setBackgroundColor:cChillPurple];
    [header addSubview:purpleLine];
    
    purpleLine = [[UIView alloc] initWithFrame:CGRectMake(0, kProfileTableViewHeaderHeight - 1.0f, kScreenWidth, 1.0f)];
    [purpleLine setBackgroundColor:cChillPurple];
    [header addSubview:purpleLine];
    
    purpleLine = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth / 2.0f - 0.5f, kProfileTableViewHeaderHeight - 34.0f, 1.0f, 33.0f)];
    [purpleLine setBackgroundColor:cChillPurple];
    [header addSubview:purpleLine];
    
    
    UILabel *sessionCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kProfileTableViewHeaderHeight - 35.0f, kScreenWidth / 2.0f - 40.0f, 35.0f)];
    [sessionCountLabel setFont:fHNBI(15)];
    [sessionCountLabel setTextColor:cChillPurple];
    [sessionCountLabel setTextAlignment:NSTextAlignmentRight];
    [sessionCountLabel setText:[NSString stringWithFormat:@"Sessions : %li", (long)[user[@"numberOfSessions"] integerValue]]];
    
    [header addSubview:sessionCountLabel];
    
    
    UILabel *timeSpentLabel = [[UILabel alloc] initWithFrame:CGRectMake(kScreenWidth / 2.0f + 40.0f, kProfileTableViewHeaderHeight - 35.0f, kScreenWidth / 2.0f - 40.0f, 35.0f)];
    [timeSpentLabel setFont:fHNBI(15)];
    [timeSpentLabel setTextColor:cChillPurple];
    [timeSpentLabel setTextAlignment:NSTextAlignmentLeft];
    [sessionCountLabel setText:[NSString stringWithFormat:@"Time : %lih%li", (long)([user[@"timeSpent"] integerValue] / 60), (long)([user[@"timeSpent"] integerValue] % 60)]];
    
    [header addSubview:timeSpentLabel];
    
    
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChillSessionCellIdentifier";
    
    ChillSessionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) cell = [[ChillSessionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    [cell setSession:[sessions objectAtIndex:[indexPath row]] upSession:[indexPath row] == 0 ? nil : [sessions objectAtIndex:[indexPath row] - 1] downSession:[indexPath row] + 1 == sessions.count ? nil : [sessions objectAtIndex:[indexPath row] + 1] userHidden:YES];
    
    [cell setLikesClicked:^(Session *session) {
        likesClicked(session);
    }];
    
    return cell;
}


@end
