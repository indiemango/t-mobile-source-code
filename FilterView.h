//
//  FilterView.h
//  Represent
//
//  Created by Tomáš Hromada on 26/01/15.
//  Copyright (c) 2015 com.represent.represent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterView : UIView < UIScrollViewDelegate >

@property (nonatomic) UIButton *done;
@property (nonatomic) e_sortType sortType;
@property (nonatomic) e_campaignType campaignType;

- (void) reset;

@end
